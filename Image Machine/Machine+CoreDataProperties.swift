//
//  Machine+CoreDataProperties.swift
//  Image Machine
//
//  Created by Afriyandi Setiawan on 9/10/16.
//  Copyright © 2016 Afriyandi Setiawan. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Machine {

    @NSManaged var machineID: NSNumber?
    @NSManaged var machineName: String?
    @NSManaged var machineType: String?
    @NSManaged var machineBarcode: NSNumber?
    @NSManaged var lastMaintenance: NSDate?

}
